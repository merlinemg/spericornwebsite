$(document).ready(function () {
	
	
	
	//$('#background').mouseParallax({ moveFactor: 5 });
	//$('#foreground').mouseParallax({ moveFactor: 3 });
	//$('#fore-foreground').mouseParallax({ moveFactor: 7 });
	//$('#fore-fore-foreground').mouseParallax({ moveFactor: 12 });
	
	function homeSliderTest(){
		$('.sliderText-carousel').owlCarousel({
			items:1,
			loop:true,
			margin:10,
			autoplay:true,
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			autoplayTimeout:3000,
			autoplayHoverPause:false
		});
	}
	
	
	
	
	$(function(){
			
		if (screen.width > 1099) {
			$('.loadSlider').load('homeSlider.php', function(){
				$('#background').mouseParallax({ moveFactor: 5 });
				$('#foreground').mouseParallax({ moveFactor: 3 });
				$('#fore-foreground').mouseParallax({ moveFactor: 7 });
				$('#fore-fore-foreground').mouseParallax({ moveFactor: 12 });
				homeSliderTest();
			});
		} else {
			$('.loadSlider').load('homeSlider2.php', function(){
				homeSliderTest();
			});
		}
			
	});
	
	
	
	$(window).one('scroll',function() {
		$('.homeSocial').load('socialCall.php', function(){
			
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
			
			!function(a,b,c){var d,e,f;d="PIN_"+~~((new Date).getTime()/864e5),a[d]?a[d]+=1:(a[d]=1,a.setTimeout(function(){e=b.getElementsByTagName("SCRIPT")[0],f=b.createElement("SCRIPT"),f.type="text/javascript",f.async=!0,f.src=c.mainUrl+"?"+Math.random(),e.parentNode.insertBefore(f,e)},10))}(window,document,{mainUrl:"//assets.pinterest.com/js/pinit_main.js"});
			
		});
		
	});
		
	
	
	
	
	$('.clients-carousel').owlCarousel({
		margin: 40,
		dots: false,
		navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			600:{
				items:1,
				nav:true,
			},
			1000:{
				items:2,
				nav:true,
			}
		}
	});
	
	$('.blogPosts-carousel').owlCarousel({
		loop: true,
		margin: 30,
		navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:false
			},
			599:{
				items:1,
				nav:false
			},
			750:{
				items:2,
				nav:false
			},
			1000:{
				items:2,
				nav:false,
				loop:false
			},
			1199:{
				items:3,
				nav:false,
				loop:false
			},
			1400:{
				items:4,
				nav:false,
				loop:false
			}
		}
	});
	
	
	
	
	
	$(document).on('click', '.promoRight', function(e){
		
		if( !$('.video-background').hasClass('megaShow') )
			{
				$this = $(this);
				$this.find('.video-background').addClass('megaShow');
				//$("#existing-iframe-example")[0].src += "&autoplay=1";
				
				e.preventDefault();
				var URL = $(this).find('.video-foreground').attr('data-link');
				var htm = '<iframe width="425" height="349" src="http://www.youtube.com/embed/' + URL + '?rel=0&autoplay=1&showinfo=0" frameborder="0"  allowfullscreen ></iframe><div class="closeMegaShow">×</div>';

				$('.video-foreground').html(htm);

				return false;
			}

	});
	
	$(document).on('click', '.closeMegaShow', function(e){ 

		$this = $(this);
		$this.closest('.video-background').removeClass('megaShow');
		$('.video-foreground').html('');
		e.preventDefault();
		return false;
		
	});
	
	
	
	//Category filter
	(function ( $ ) {
		$.fn.categoryFilter=function(selector){
			this.click( function() {
				var categoryValue = $(this).attr('data-filter');
				
				$this= $(this);
				$this.closest('ul').find('li').each(function(){
					$(this).find('a').removeClass('active');
				});
				$this.addClass('active')
				
				if(categoryValue=="all") {
					$('.filter').show(1000);
				} else {
					$(".filter").not('.'+categoryValue).hide('3000');
					$('.filter').filter('.'+categoryValue).show('3000');
				}
			});
		}
	}( jQuery ));
	
	
	$('.category-filter .category-button').categoryFilter();
	
	
	
	$(window).scroll(function() {    
		var scroll = $(window).scrollTop();

		if (scroll >= 250) {
			$(".sHeader").addClass("shrinked");
		} else {
			$(".sHeader").removeClass("shrinked");
		}
	});
	
	
	
	$(".goToRound").on('click', function(e) {

		
		$('html,body').animate({
        scrollTop: $("#sectionForm").offset().top - 150},
        'slow');
		
		e.preventDefault();
		
		
	});
	

	//To make URL Active
	$(function(){
		var CURRENT_URL = window.location.href.split('#')[0].split('?')[0];
		//alert( CURRENT_URL );
		$('#navbarDefault').find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('active');
	})
	
	
	
	

});










