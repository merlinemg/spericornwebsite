"""spericorn URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import django
from django.conf.urls import url
from django.views.static import serve
from django.contrib import admin
from django.views.decorators.gzip import gzip_page
from home.views import HomeView, AboutUsView, WebAppDevelopView, MobileAppDevelopment, DigitalMarketing, \
    CustomerSupport, AngularJs, ContactUs, Technologies, PHP, Sass, Ionic, Java, WordPress, Python, StaffAugmentation, \
    ColdFusion, Services, Django, NodeJs, iOs, RubyOnRails, Laravel, ReactJs, Android, MoreServices,SocialCall
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', gzip_page(HomeView.as_view()), name='home'),
    url(r'^about-us', AboutUsView.as_view(), name='about_us'),
    url(r'^services/web-app-development', WebAppDevelopView.as_view(), name='web_app_develop'),
    url(r'^services/mobile-app-development', MobileAppDevelopment.as_view(), name='mobile_app_develop'),
    url(r'^services/digital-marketing', DigitalMarketing.as_view(), name='digital_marketing'),
    url(r'^services/customer-support', CustomerSupport.as_view(), name='customer_support'),
    url(r'^services/staff-augmentation', StaffAugmentation.as_view(), name='staff_augmentation'),
    url(r'^services/more-service', MoreServices.as_view(), name='more-service'),
    url(r'^socialCall', SocialCall.as_view(), name='social_call'),
    url(r'^services', Services.as_view(), name='services'),
    url(r'^technologies/', Technologies.as_view(), name='technologies'),
    url(r'^technology/ColdFusion/', ColdFusion.as_view(), name='coldfusion'),
    url(r'^technology/Django/', Django.as_view(), name='django'),
    url(r'^technology/NodeJs/', NodeJs.as_view(), name='node_js'),
    url(r'^technology/RubyOnRails/', RubyOnRails.as_view(), name='ruby_on_rails'),
    url(r'^technology/Laravel/', Laravel.as_view(), name='laravel'),
    url(r'^technology/ReactJs/', ReactJs.as_view(), name='react_js'),
    url(r'^technology/Android/', Android.as_view(), name='android'),
    url(r'^technology/iOS/', iOs.as_view(), name='iOS'),
    url(r'^technology/Php/', PHP.as_view(), name='php'),
    url(r'^technology/AngularJs/', AngularJs.as_view(), name='angular_js'),
    url(r'^technology/Python/', Python.as_view(), name='python'),
    url(r'^technology/WordPress/', WordPress.as_view(), name='wordpress'),
    url(r'^technology/Sass/', Sass.as_view(), name='sass'),
    url(r'^technology/Java/', Java.as_view(), name='java'),
    url(r'^technology/ionic/', Ionic.as_view(), name='ionic'),
    url(r'^contact-us/', ContactUs.as_view(), name='contact_us'),
    url(r'^static/(?P<path>.*)$', serve,
                            {'document_root': settings.STATIC_ROOT, 'show_indexes': False})
]
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# urlpatterns += staticfiles_urlpatterns()


handler404 = 'home.views.handler404'