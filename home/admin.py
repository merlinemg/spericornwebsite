# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from home.models import FreeQuotes, HireADeveloperQuotes

admin.site.register(FreeQuotes)
admin.site.register(HireADeveloperQuotes)
