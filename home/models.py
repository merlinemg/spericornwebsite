# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class FreeQuotes(models.Model):
    name = models.CharField(max_length=500,null=True,blank=True)
    email_id = models.EmailField(max_length=500)
    phone_number = models.CharField(max_length=500,null=True,blank=True)
    message = models.CharField(max_length=1000)

    def __str__(self):
        return self.name



class HireADeveloperQuotes(models.Model):
    user_name = models.CharField(max_length=500,null=True,blank=True)
    email = models.EmailField(max_length=500)
    phone_number = models.CharField(max_length=500,null=True,blank=True)
    message = models.CharField(max_length=1000)

    def __str__(self):
        return self.user_name
