# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import threading

from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from home.forms import UserQuickAddForm, HireADeveloperQuickAddForm
from home.models import HireADeveloperQuotes
from htmlmin.decorators import minified_response


class HomeView(TemplateView):
    template_name = 'index.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        html_response = render_to_string(self.template_name)
        # return HttpResponse(html_response)
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        response_data = {}
        form = UserQuickAddForm(request.POST)
        if form.is_valid():
            response_data['status'] = True
            form.save()
            message_body = 'Name :' + ' ' + request.POST.get('name') + '\n' + 'Email: ' + request.POST.get(
                'email_id') + '\n\n'

            def Email():
                send_mail('Free Quote Message From ' + request.POST.get('name'),
                          message_body + request.POST.get('message'), 'info@spericorn.com',
                          ['info@spericorn.com', 'amithfd2@gmail.com'])

            send_user_mail = threading.Thread(name='Email', target=Email)
            send_user_mail.start()
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
        if request.POST.get('ajax_key') == 'hiredeveloper':
            form = HireADeveloperQuickAddForm(request.POST)
            if form.is_valid():
                response_data['status'] = True
                form.save()
                message_body = 'Name :' + ' ' + request.POST.get('user_name') + '\n' + 'Email: ' + request.POST.get(
                    'email') + '\n\n'


                def Email():
                    send_mail('Hire a Developer From ' + request.POST.get('user_name'),
                              message_body + request.POST.get('message'), 'info@spericorn.com',
                              ['info@spericorn.com', 'amithfd2@gmail.com'])

                send_user_mail = threading.Thread(name='Email', target=Email)
                send_user_mail.start()
            else:
                response_data['status'] = False
                response_data['errors'] = form.errors

        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AboutUsView(TemplateView):
    template_name = 'about.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class WebAppDevelopView(TemplateView):
    template_name = 'serviceWebAppDevelopment.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class MobileAppDevelopment(TemplateView):
    template_name = 'serviceMobileAppDevelopment.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class DigitalMarketing(TemplateView):
    template_name = 'serviceDigitalMarketing.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class CustomerSupport(TemplateView):
    template_name = 'serviceCustomerSupport.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class StaffAugmentation(TemplateView):
    template_name = 'serviceStaffAugmentation.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Services(TemplateView):
    template_name = 'services.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class MoreServices(TemplateView):
    template_name = 'serviceMore.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class ColdFusion(TemplateView):
    template_name = 'technologyColdfusion.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Django(TemplateView):
    template_name = 'technologyDjango.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class NodeJs(TemplateView):
    template_name = 'technologyNodejs.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class iOs(TemplateView):
    template_name = 'technologyIos.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class RubyOnRails(TemplateView):
    template_name = 'technologyRubyonrails.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Laravel(TemplateView):
    template_name = 'technologyLaravel.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class ReactJs(TemplateView):
    template_name = 'technologyReactjs.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Android(TemplateView):
    template_name = 'technologyAndroid.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class AngularJs(TemplateView):
    template_name = 'technologyAngularjs.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Sass(TemplateView):
    template_name = 'technologySass.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Ionic(TemplateView):
    template_name = 'technologyIonic.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Java(TemplateView):
    template_name = 'technologyJava.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class WordPress(TemplateView):
    template_name = 'technologyWordpress.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Python(TemplateView):
    template_name = 'technologyPython.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class PHP(TemplateView):
    template_name = 'technologyPHP.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class SocialCall(TemplateView):
    template_name = 'socialCall.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class Technologies(TemplateView):
    template_name = 'technology.html'

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class ContactUs(TemplateView):
    template_name = 'contact.html'

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ContactUs, self).dispatch(*args, **kwargs)

    @minified_response
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        response_data = {}
        form = UserQuickAddForm(request.POST)
        if form.is_valid():
            response_data['status'] = True
            form.save()
            message_body = 'Name :' + ' ' + request.POST.get('name') + '\n' + 'Email: ' + request.POST.get(
                'email_id') + '\n\n'

            def Email():
                send_mail('Free Quote Message From ' + request.POST.get('name'),
                          message_body + request.POST.get('message'), 'info@spericorn.com',
                          ['info@spericorn.com', 'amithfd2@gmail.com'])

            send_user_mail = threading.Thread(name='Email', target=Email)

            send_user_mail.start()

        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
        return HttpResponse(json.dumps(response_data), content_type="application/json")

def handler404(request):
    return redirect('/')

