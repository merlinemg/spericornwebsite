from django import forms

from home.models import FreeQuotes, HireADeveloperQuotes


class UserQuickAddForm(forms.ModelForm):
    class Meta:
        model = FreeQuotes
        fields = '__all__'


class HireADeveloperQuickAddForm(forms.ModelForm):
    class Meta:
        model = HireADeveloperQuotes
        fields = '__all__'
