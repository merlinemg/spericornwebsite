# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-01 10:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_auto_20171101_1024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hireadeveloperquotes',
            name='job_category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='category', to='home.Jobcategory'),
            preserve_default=False,
        ),
    ]
